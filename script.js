
// Регистрируем обработчик событий на изменения объекта select: при выборе того или иного параметра регистрируется 
// выбранный параметр и передается в фунцию "disableInput" для тоого, чтобы заблокировать возможность ввода выбранного параметра
// При срабатывании данного обработчика мы удаляем аттрибут "disabled" для того, чтобы вернуться к исходному состоянию
// Функцию "validate" внутри обработчика используем для того, чтобы сообщить пользователю о невведенных значениях параметров 
let select = document.getElementById("param");
select.addEventListener("change", function () {
   let choice = select.value;
   buttonGetResult.removeAttribute("disabled");
   document.querySelector(".result").innerHTML = '';
   disableInput(choice);
   validate();
});


// Метод массива "forEach" используем для того, чтобы пройтись по всем найденным, не заблокированным полям ввода и проверить поля 
// параметров на "заполненность"
// При пустоп значении - удаляем, а при заполненном поле - добавляем класс "hide", который скрывает валидацию
const valueContainers = document.querySelectorAll('.values-container');
valueContainers.forEach((el) => {

   const inputValue = el.querySelector('.param-value');
   inputValue.addEventListener('input', function () {
      const err = el.querySelector('.error');
      if (inputValue.value.length > 0) {
         err.classList.add("hide");
      } else {
         err.classList.remove("hide");
      }
   });
});


// Функция "validate" предназначена для валидации. Функция находит все незаблокированные поля ввода, проверяется поля на заполненность
// и в случае, если поле пустое, добавляется валидация. В начале функции, вся валидация удаляется
function validate() {

   valueContainers.forEach((el) => {

      el.querySelector('.error').textContent = '';

      if (el.querySelector('.param-value') && !el.querySelector('.param-value').value) {

         el.querySelector('.error').textContent = 'Введите значение параметра';

      }
   });
};

//    for (let i = 0; i < errors.length; i++) {
//       errors[i].remove()
//    };

//    for (let i = 0; i < fields.length; i++) {
//       if (!fields[i].value) {

//          let error = document.createElement('div')
//          error.className = 'error'
//          error.style.color = 'red'
//          error.innerHTML = 'Введите значение параметра'
//          fields[i].parentElement.appendChild(error)

//       }
//    };
// };



// Функция "disableInput" принимает в качестве параметра выбранный для расчета параметр, в начале вызывается функция удаления
// блокировки поля ввода, после чего находит элемент input в HTML разметке блокирует, а также имзеняет класс, для валидации
// После чего вызывается функция создания HtML тэга, который предназачен в качестве подсказки пользователю
function disableInput(choise) {
   removeDisable();
   const selectedInput = document.getElementById(`${choise}-value`);
   selectedInput.value = "";
   selectedInput.setAttribute("disabled", "disabled");
   selectedInput.className += "disabled";
   // createTag();
};


// Функция "removeDisable" используется для того, чтобы найти заблокированный input разблокировать и вернуть исходный класс 
// для валидации
function removeDisable() {
   const inputs = document.querySelectorAll('.values-block .param-valuedisabled');
   inputs.forEach((el) => {
      el.removeAttribute("disabled");
      el.className = "param-value";
   });
};


// Функция "createTag" предназначена для добавления HTML тега "h2"
// function createTag() {
//    let newDiv = document.createElement("div");
//    newDiv.innerHTML = "<h2>Введите значения параметров</h2>";
//    const titleBlock = document.getElementById("title");
//    titleBlock.innerHTML = "";
//    titleBlock.insertAdjacentElement("afterbegin", newDiv);
// };


// Обработчик событий, который срабатывает по клику на кнопку "Рассчитать" в начале применяет валидацию, после чего, считывает 
// введенные значения параметров и с помощью конструкции "switch-case" отбирает функцию для расчета выбранного параметра и
//  в блок с результатами выдает полученное значение расчета
buttonGetResult.onclick = function () {

   validate();

   const resultBLock = document.querySelector(".result");
   const r = 8.31;
   const p = document.getElementById("pressure-value").value;
   const v = document.getElementById("vol-value").value;
   const m = document.getElementById("massa-value").value;
   const mm = document.getElementById("m-massa-value").value;
   const t = document.getElementById("temperature-value").value;
   switch (select.value) {
      case "pressure":
         resultBLock.innerHTML = getP(r, v, m, mm, t);
         break;
      case "vol":
         resultBLock.innerHTML = getV(r, p, m, mm, t);
         break;
      case "massa":
         resultBLock.innerHTML = getM(r, p, v, mm, t);
         break;
      case "m-massa":
         resultBLock.innerHTML = getMm(r, p, v, m, t);
         break;
      case "temperature":
         resultBLock.innerHTML = getT(r, p, v, m, mm);
         break;
   }
};


// Функции расчета каждого параметра по отдельности с проверкой на корректность полученного результата
let incorrect = "Некорректно введены значения параметров!";

function getP(r, v, m, mm, t) {

   let result = ((m / mm) * r * t) / v;

   if ((isNaN(result)) || (result == null) || (!isFinite(result))) {
      return incorrect;
   } else {
      return result + " Па";
   }
};

function getV(r, p, m, mm, t) {

   let result = ((m / mm) * r * t) / p;

   if ((isNaN(result)) || (result == null) || (!isFinite(result))) {
      return incorrect;
   } else {
      return result + " м3";
   }
};

function getM(r, p, v, mm, t) {

   let result = (p * v * mm) / (r * t);

   if ((isNaN(result)) || (result == null) || (!isFinite(result))) {
      return incorrect;
   } else {
      return result + " кг";
   }
};

function getMm(r, p, v, m, t) {

   let result = (m * r * t) / (p * v);

   if ((isNaN(result)) || (result == null) || (!isFinite(result))) {
      return incorrect;
   } else {
      return result + " кг/моль";
   }
};

function getT(r, p, v, m, mm) {

   let result = (p * v * mm) / (r * m);

   if ((isNaN(result)) || (result == null) || (!isFinite(result))) {
      return incorrect;
   } else {
      return result + " К";
   }
};


